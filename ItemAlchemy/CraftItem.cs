﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ItemAlchemy
{
    class CraftItem
    {
        int coalCount = 0;  //石炭
        int rodCount = 0;   //棒
        int woolCount = 0;  //羊毛
        int woodCount = 0;  //木材
        int ironCount = 0;  //鉄
        int yarnCount = 0;  //糸
        int wheatCount = 0; //小麦
        int cacaoCount = 0; //カカオ
        int glassCount = 0; //ガラス
        int brickCount = 0; //レンガ

        public int CoalCount { get => coalCount; set => coalCount = value; }
        public int RodCount { get => rodCount; set => rodCount = value; }
        public int WoolCount { get => woolCount; set => woolCount = value; }
        public int WoodCount { get => woodCount; set => woodCount = value; }
        public int IronCount { get => ironCount; set => ironCount = value; }
        public int YarnCount { get => yarnCount; set => yarnCount = value; }
        public int WheatCount { get => wheatCount; set => wheatCount = value; }
        public int CacaoCount { get => cacaoCount; set => cacaoCount = value; }
        public int GlassCount { get => glassCount; set => glassCount = value; }
        public int BrickCount { get => brickCount; set => brickCount = value; }

        //public override bool Equals(object obj)
        //{
        //    CraftItem craftItem = obj as CraftItem;

        //    //変換失敗ならそもそも等価ではない
        //    if (craftItem == null)
        //    {
        //        return false;
        //    }

        //    //変換できていた場合、各元素の値が等しければ等価
        //    return craftItem.coalCount == coalCount
        //        && craftItem.rodCount == rodCount
        //        && craftItem.woolCount == woolCount
        //        && craftItem.ironCount == ironCount
        //        && craftItem.yarnCount == yarnCount
        //        && craftItem.wheatCount == wheatCount
        //        && craftItem.cacaoCount == cacaoCount
        //        && craftItem.glassCount == glassCount
        //        && craftItem.brickCount == brickCount;
        //}
    }
}
