﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ItemAlchemy
{
    public partial class Form1 : Form
    {
        Dictionary<string, int> material = new Dictionary<string, int>();
        List<Item> items = new List<Item>();

        public Form1()
        {
            InitializeComponent();
            ReadMaterialFile();
            ReadItemFile();
        }
        //材料（material.txt）の読み込みメソッド
        private void ReadMaterialFile()
        {
            using (System.IO.StreamReader file =
                new System.IO.StreamReader(@"..\..\material.txt"))
            {
                while (!file.EndOfStream)
                {
                    string line = file.ReadLine();
                    string[] data = line.Split(',');
                    this.material.Add(data[0], int.Parse(data[1]));
                }
            }
        }
        //アイテム（data.txt）の読み込みメソッド
        private void ReadItemFile()
        {
            using (System.IO.StreamReader file =
                new System.IO.StreamReader(@"..\..\data.txt"))
            {
                while (!file.EndOfStream)
                {
                    string line = file.ReadLine();
                    string[] data = line.Split(',');
                    this.items.Add(new Item(int.Parse(data[0]), data[1], data[2], data[3]));
                }
            }
        }
        private int Alchemy() //メモ：override equals
        {
            int coalCount = 0;  //石炭
            int rodCount = 0;   //棒
            int woolCount = 0;  //羊毛
            int woodCount = 0;  //木材
            int ironCount = 0;  //鉄
            int yarnCount = 0;  //糸
            int wheatCount = 0; //小麦
            int cacaoCount = 0; //カカオ
            int glassCount = 0; //ガラス
            int brickCount = 0; //レンガ

            foreach (string s in addMaterialList.Items)
            {
                switch (s)
                {
                    case "石炭":
                        coalCount++;
                        break;
                    case "棒":
                        rodCount++;
                        break;
                    case "羊毛":
                        woolCount++;
                        break;
                    case "木材":
                        woodCount++;
                        break;
                    case "鉄":
                        ironCount++;
                        break;
                    case "糸":
                        yarnCount++;
                        break;
                    case "小麦":
                        wheatCount++;
                        break;
                    case "カカオ":
                        cacaoCount++;
                        break;
                    case "ガラス":
                        glassCount++;
                        break;
                    case "レンガ":
                        brickCount++;
                        break;
                }
            }
            //条件分岐
            if (coalCount == 1 && rodCount == 1)
            {
                return 0; //松明
            }
            else if (woolCount == 3 && woodCount == 3)
            {
                return 1; //ベッド
            }
            else if (ironCount == 3 && rodCount == 2)
            {
                return 2; //斧
            }
            else if (ironCount == 3)
            {
                return 3; //バケツ
            }
            else if (rodCount == 3 && yarnCount == 2)
            {
                return 4; //釣竿
            }
            else if (wheatCount == 2 && cacaoCount == 1)
            {
                return 5; //クッキー
            }
            else if (wheatCount == 3)
            {
                return 6; //パン
            }
            else if (glassCount == 3)
            {
                return 7; //ガラス瓶
            }
            else if (rodCount == 8 && woolCount == 1)
            {
                return 8; //絵画
            }
            else if (brickCount == 3)
            {
                return 9; //植木鉢
            }
            else if (rodCount == 7)
            {
                return 10; //はしご
            }
            else //全ての条件に当てはまらない場合
            {
                return 11; //ゴミ
            }
        }

        //Form1を読み込んだとき
        private void MaterialList_Load(object sender, EventArgs e)
        {
            //materialListに材料名を表示する
            foreach (KeyValuePair<string, int> mtral in material)
            {
                this.materialList.Items.Add(mtral.Key);
            }
        }
        //追加ボタンを押したとき 材料が選択されていない場合の処理
        private void AddMaterialClicked(object sender, EventArgs e)
        {
            addMaterialList.Items.Add(materialList.SelectedItem); //選択した項目をaddMaterialListに追加
        }
        //合成ボタンを押したとき 材料がリストに載っていない場合の処理
        private void ItemAlchemyClicked(object sender, EventArgs e)
        {
            int m = 0;
            m = Alchemy();
            //値に応じたアイテムを表示
            itemNameLabel.Text = items[m].Name;
            itemPictureBox.Image = Image.FromFile(@"../../img/" + items[m].Img + ".png");
            itemDescLabel.Text = items[m].Desc;
        }
        //削除ボタンを押したとき 材料が選択されていない場合の処理
        private void RemoveClicked(object sender, EventArgs e)
        {
            addMaterialList.Items.Remove(addMaterialList.SelectedItem); //選択した項目を削除
        }
        //リセットボタンを押したとき
         private void ResetClicked(object sender, EventArgs e)
        {
            addMaterialList.Items.Clear();
        }
    }
}
