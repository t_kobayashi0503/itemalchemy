﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ItemAlchemy
{
    class Item
    {
        private int id; //アイテムID
        private string name; //アイテム名
        private string img;  //アイテム画像URL
        private string desc; //アイテム説明

        //コンストラクタ
        public Item(int id, string name, string img, string desc)
        {
            this.Id = id;
            this.Name = name;
            this.Img = img;
            this.Desc = desc;
        }

        //カプセル化
        public int Id { get => id; set => id = value; }
        public string Name { get => name; set => name = value; }
        public string Img { get => img; set => img = value; }
        public string Desc { get => desc; set => desc = value; }
    }
}
