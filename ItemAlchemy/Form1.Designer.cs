﻿namespace ItemAlchemy
{
    partial class Form1
    {
        /// <summary>
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージド リソースを破棄する場合は true を指定し、その他の場合は false を指定します。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows フォーム デザイナーで生成されたコード

        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.materialList = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.addMaterialButton = new System.Windows.Forms.Button();
            this.itemAlchemyButton = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.addMaterialList = new System.Windows.Forms.ListBox();
            this.itemPictureBox = new System.Windows.Forms.PictureBox();
            this.itemNameLabel = new System.Windows.Forms.Label();
            this.itemDescLabel = new System.Windows.Forms.Label();
            this.removeButton = new System.Windows.Forms.Button();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.resetButton = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.itemPictureBox)).BeginInit();
            this.flowLayoutPanel1.SuspendLayout();
            this.flowLayoutPanel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // materialList
            // 
            this.materialList.FormattingEnabled = true;
            this.materialList.ItemHeight = 12;
            this.materialList.Location = new System.Drawing.Point(27, 47);
            this.materialList.Name = "materialList";
            this.materialList.Size = new System.Drawing.Size(189, 136);
            this.materialList.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label1.Location = new System.Drawing.Point(27, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(72, 16);
            this.label1.TabIndex = 4;
            this.label1.Text = "材料一覧";
            // 
            // addMaterialButton
            // 
            this.addMaterialButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.addMaterialButton.Location = new System.Drawing.Point(222, 47);
            this.addMaterialButton.Name = "addMaterialButton";
            this.addMaterialButton.Size = new System.Drawing.Size(66, 49);
            this.addMaterialButton.TabIndex = 5;
            this.addMaterialButton.Text = "追加";
            this.addMaterialButton.UseVisualStyleBackColor = false;
            this.addMaterialButton.Click += new System.EventHandler(this.AddMaterialClicked);
            // 
            // itemAlchemyButton
            // 
            this.itemAlchemyButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.itemAlchemyButton.ForeColor = System.Drawing.Color.White;
            this.itemAlchemyButton.Location = new System.Drawing.Point(222, 227);
            this.itemAlchemyButton.Name = "itemAlchemyButton";
            this.itemAlchemyButton.Size = new System.Drawing.Size(66, 51);
            this.itemAlchemyButton.TabIndex = 8;
            this.itemAlchemyButton.Text = "合成";
            this.itemAlchemyButton.UseVisualStyleBackColor = false;
            this.itemAlchemyButton.Click += new System.EventHandler(this.ItemAlchemyClicked);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label2.Location = new System.Drawing.Point(27, 203);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 16);
            this.label2.TabIndex = 7;
            this.label2.Text = "作業台";
            // 
            // addMaterialList
            // 
            this.addMaterialList.FormattingEnabled = true;
            this.addMaterialList.ItemHeight = 12;
            this.addMaterialList.Location = new System.Drawing.Point(27, 227);
            this.addMaterialList.Name = "addMaterialList";
            this.addMaterialList.Size = new System.Drawing.Size(189, 136);
            this.addMaterialList.TabIndex = 6;
            // 
            // itemPictureBox
            // 
            this.itemPictureBox.BackColor = System.Drawing.Color.White;
            this.itemPictureBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.itemPictureBox.Location = new System.Drawing.Point(315, 47);
            this.itemPictureBox.Name = "itemPictureBox";
            this.itemPictureBox.Size = new System.Drawing.Size(255, 244);
            this.itemPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.itemPictureBox.TabIndex = 9;
            this.itemPictureBox.TabStop = false;
            // 
            // itemNameLabel
            // 
            this.itemNameLabel.AutoSize = true;
            this.itemNameLabel.Font = new System.Drawing.Font("MS UI Gothic", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.itemNameLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.itemNameLabel.Location = new System.Drawing.Point(3, 0);
            this.itemNameLabel.Name = "itemNameLabel";
            this.itemNameLabel.Size = new System.Drawing.Size(105, 22);
            this.itemNameLabel.TabIndex = 10;
            this.itemNameLabel.Text = "アイテム名";
            this.itemNameLabel.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // itemDescLabel
            // 
            this.itemDescLabel.AutoSize = true;
            this.itemDescLabel.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.itemDescLabel.Location = new System.Drawing.Point(3, 0);
            this.itemDescLabel.Name = "itemDescLabel";
            this.itemDescLabel.Size = new System.Drawing.Size(66, 12);
            this.itemDescLabel.TabIndex = 11;
            this.itemDescLabel.Text = "アイテム説明";
            // 
            // removeButton
            // 
            this.removeButton.Font = new System.Drawing.Font("MS UI Gothic", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.removeButton.Location = new System.Drawing.Point(222, 284);
            this.removeButton.Name = "removeButton";
            this.removeButton.Size = new System.Drawing.Size(66, 25);
            this.removeButton.TabIndex = 12;
            this.removeButton.Text = "項目削除";
            this.removeButton.UseVisualStyleBackColor = true;
            this.removeButton.Click += new System.EventHandler(this.RemoveClicked);
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.itemDescLabel);
            this.flowLayoutPanel1.Location = new System.Drawing.Point(317, 297);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(252, 84);
            this.flowLayoutPanel1.TabIndex = 13;
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.flowLayoutPanel2.Controls.Add(this.itemNameLabel);
            this.flowLayoutPanel2.Location = new System.Drawing.Point(315, 10);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(254, 34);
            this.flowLayoutPanel2.TabIndex = 14;
            // 
            // resetButton
            // 
            this.resetButton.Location = new System.Drawing.Point(222, 315);
            this.resetButton.Name = "resetButton";
            this.resetButton.Size = new System.Drawing.Size(66, 25);
            this.resetButton.TabIndex = 15;
            this.resetButton.Text = "リセット";
            this.resetButton.UseVisualStyleBackColor = true;
            this.resetButton.Click += new System.EventHandler(this.ResetClicked);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label3.Location = new System.Drawing.Point(591, 12);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(110, 16);
            this.label3.TabIndex = 0;
            this.label3.Text = "【アイテムレシピ】";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(596, 38);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(44, 132);
            this.label4.TabIndex = 16;
            this.label4.Text = "松明\r\nベッド\r\n斧\r\nバケツ\r\n釣竿\r\nクッキー\r\nパン\r\nガラス瓶\r\n絵画\r\n植木鉢\r\nはしご";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(646, 38);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(88, 132);
            this.label5.TabIndex = 17;
            this.label5.Text = "石炭*1、棒*1\r\n羊毛*3、木材*3\r\n鉄*3、棒*2\r\n鉄*3\r\n棒*3、糸*2\r\n小麦*2、カカオ*1\r\n小麦*3\r\nガラス*3\r\n棒*8、羊毛*1\r\nレン" +
    "ガ*3\r\n棒*7";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(759, 386);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.resetButton);
            this.Controls.Add(this.flowLayoutPanel2);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this.removeButton);
            this.Controls.Add(this.itemPictureBox);
            this.Controls.Add(this.itemAlchemyButton);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.addMaterialList);
            this.Controls.Add(this.addMaterialButton);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.materialList);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.MaterialList_Load);
            ((System.ComponentModel.ISupportInitialize)(this.itemPictureBox)).EndInit();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel1.PerformLayout();
            this.flowLayoutPanel2.ResumeLayout(false);
            this.flowLayoutPanel2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox materialList;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button addMaterialButton;
        private System.Windows.Forms.Button itemAlchemyButton;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ListBox addMaterialList;
        private System.Windows.Forms.PictureBox itemPictureBox;
        private System.Windows.Forms.Label itemNameLabel;
        private System.Windows.Forms.Label itemDescLabel;
        private System.Windows.Forms.Button removeButton;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private System.Windows.Forms.Button resetButton;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
    }
}

